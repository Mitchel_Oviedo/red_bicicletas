var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

describe('Bicleta API', ()=>{
    describe('GET BICICLETAS /', ()=>{
        it('Status 200', () =>{
            expect(Bicicleta.allBicis.length).toBe(0);

            var a =  new Bicicleta(1,'negro', 'montaña', [2.9373, -75.28225]);
            Bicicleta.add(a);

            request.get('http://localhost:5000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });
});